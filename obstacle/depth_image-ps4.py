# !/usr/bin/env python
import rospy
import numpy as np
from sensor_msgs.msg import Image
from stereo_msgs.msg import DisparityImage
import cv2
from cv_bridge import CvBridge, CvBridgeError


bridge = CvBridge()

'''def left_callback(left):
	global b, x1, y1
	left_arr = bridge.imgmsg_to_cv2(left, desired_encoding="bgr8")
	for i in range(1,len(b)-1):
		x1 = b[i,0]
		y1 = b[i,1]
		cv2.circle(left_arr, (x1, y1),5, (0, 255, 0), -1)
	cv2.imshow("left", left_arr)
	if cv2.waitKey(1) and 0xFF == ord('q'):
		pass'''

def callback(data):
	global b,x1,y1
	image_arr = bridge.imgmsg_to_cv2(data.image, desired_encoding="8UC1") # Converts the disparity image into a 640 X 480 grid
	#image_arr[image_arr==0] = 1;
	focal = data.f
	base = data.T
	#depth_arr = focal*base/image_arr
	#median = cv2.medianBlur(image_arr,5)
	#blur = cv2.bilateralFilter(image_arr,9,75,75)
	#bl = cv2.cvtColor(image_arr,cv2.COLOR_GRAY2RGB)
	#ret,thresh = cv2.threshold(image_arr,127,255,0)
 	#contours, im2 = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
	#bl1 = cv2.cvtColor(bl,cv.COLOR_GRAY2RGB, 255.0)
	
	cv2.imshow("depth", image_arr)
	#cv2.drawContours(image_arr, contours, -1, (0,255,0), 3)
	cv2.moveWindow("depth",800,500)
	if cv2.waitKey(1) and 0xFF == ord('q'):
		pass
	#print bl[100,100]
	#dist = np.min(depth_arr)
	#a = zip(*np.where(depth_arr==dist))
	#a = np.array(a)
	#np.delete(a, list(range(0, a.shape[1], 2)), axis=1)
	#b = a[:,:-1]
	#print b.shape, len(b)

	#print len(b), b.shape

def listener():
	rospy.init_node('listener', anonymous=True)
	rospy.Subscriber("/stereo/disparity", DisparityImage, callback)
	#rospy.Subscriber("/stereo/left/image_rect_color", Image, left_callback)
	rospy.spin()

if __name__ == '__main__':
	listener()	
