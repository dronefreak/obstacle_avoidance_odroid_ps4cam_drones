import rospy
import numpy as np
from matplotlib import pyplot as plt
import message_filters
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge

bridge = CvBridge()
def left_callback(left_image):
	global left_arr
	# Solve all of perception here...
	left_arr = bridge.imgmsg_to_cv2(left_image, desired_encoding="bgr8")
	left_arr = cv2.cvtColor(left_arr, cv2.COLOR_BGR2GRAY)

def right_callback(left_image):
	global right_arr
    # Solve all of perception here...
	right_arr = bridge.imgmsg_to_cv2(right_image, desired_encoding="bgr8")
	right_arr = cv2.cvtColor(right_arr, cv2.COLOR_BGR2GRAY)
global left_arr	, right_arr
stereo = cv2.StereoBM(1,16,15)
disparity = stereo.compute(left_arr,right_arr)
plt.imshow(disparity,'gray')
plt.show()


def listener():
	rospy.init_node('listener', anonymous=True)
	'''left_image_sub = message_filters.Subscriber('/stereo/left/image_rect', Image)
	right_image_sub = message_filters.Subscriber('/stereo/right/image_rect', Image)
	ts = message_filters.TimeSynchronizer([left_image_sub, right_image_sub], 10)
	ts.registerCallback(callback)'''
	rospy.Subscriber("/stereo/left/image_rect_color", Image, left_callback)
	rospy.Subscriber("/stereo/right/image_rect_color", Image, right_callback)
	rospy.spin()

if __name__ == '__main__':
	listener()	
