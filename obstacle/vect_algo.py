# !/usr/bin/env python
import rospy
import numpy as np
from sensor_msgs.msg import Image
from flyt_python import api
from geometry_msgs.msg import TwistStamped
import cv2
from cv_bridge import CvBridge, CvBridgeError
import rosbag
import math


flag = True
# Takes target co-rdinates from the user
response = raw_input("Want to provide GPS co-ordinates or use defalut values? \nPress y for yes, any other letter for default.\n")
if (response == 'y') or (response == 'Y'):
	pos_x = input("Enter target x-coordinate: ")
	pos_y = input("Enter target y-coordinate: ")
	pos_z = input("Enter target height: ")
else: 
	pos_x = 10.0
	pos_y = 2.0
	pos_z = 5.0	
nav = api.navigation()
print "Navigating to: ",pos_x, pos_y, -pos_z
nav.takeoff(pos_z)
r = 0;g = 0;b = 255;

def left_callback(image_a):
	bridge1 = CvBridge()
	left_arr = bridge1.imgmsg_to_cv2(image_a, desired_encoding="bgr8")	
	cv2.rectangle(left_arr,(c,a),(d,b),(r,g,b),1)
	cv2.putText(left_arr,"Min. Distance",(c+40,a+40), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,0,0),2)
	cv2.putText(left_arr,str(front_d),(c+40,a+55), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,0,0),2)
	cv2.imshow("left_r", left_arr)
	if cv2.waitKey(1) and 0xFF == ord('q'):
		pass


# Function to get current position of the drone
def pose(val):
	global position
	position = val.twist.linear

def callback(data):
	global nav, flag, pos_x, pos_y, pos_z, n, front_arr, a, b, c, d, r, g, b
	if(flag):

		bridge = CvBridge()
		try:
			image_arr = bridge.imgmsg_to_cv2(data, desired_encoding="8UC1") # Converts the depth image into a 640 X 400 grid
		except CvBridgeError, e:                                            # providing depth on each pixel.
			print e

		focal = data.f
		base = data.T
		image_arr[image_arr==0] = 1;
		depth_arr = focal*base/image_arr
		# Removes all zeroes from the data
		#depth_arr[depth_arr==0] = 16;		
		'''x = position.x
		y = position.y 
		yaw_g = np.arctan2((pos_x-x)/(pos_y-y))
		nav.position_set(0, 0, 0, yaw_g, 0.5, False, False, True, True)'''
		a = 130, b = 320, c = 200, d = 440
		front_arr = depth_arr[a:b, c:d]
		front_d = np.min(front_arr);
		f_count = (front_arr==front_d).sum()
		'''front_arr = image_arr[a:b, c:d]
		front_d = np.max(image_arr);
		f_count = (image_arr==front_d).sum()'''
		k = 1;
		while front_d > 1.0:
			if f_count>50:
				r = 255;g = 0;b = 0;
				print "Towards Goal:", front_d, f_count
				nav.velocity_set(0.5, 0, 0, 0, 0.5, False, False, True, True)
				return
			im_arr = focal*base/front_d
			front_d = focal*base/(im_arr-k)
			k = k+1;
			f_count = (front_arr==front_d).sum()

		
		l = 1;			
		for i in range(-50,50):
			for j in range(-50,50):
				a = 130+i, b = 260+i, c = 200+j, d = 440+j;
				front_arr = depth_arr[a:b, c:d]
				front_d = np.min(front_arr)
				f_count = (front_arr==front_d).sum()
				print "Searching...", front_d, f_count
				while front_d > 1.0:
					if f_count > 50:
						x_c = 320+j;
						x_0 = 320;
						y_c = 195 + i;
						y_0 = 200;
						yaw = np.arctan2((x_c-x_0),(y_C-y_0))
						print "Moving", front_d, f_count
						nav.velocity_set(0, 0.5*math.sin(yaw), -0.5*math.cos(yaw), 0.0, 0.5, False, False, True, True)
						return
					im_arr = focal*base/front_d
					front_d = focal*base/(im_arr-l)
					l = l+1;
					f_count = (front_arr==front_d).sum()
					
		m = 1;
		for i in range(0,270):
			for j in range(0,400):
				a = i, b = 130+i, c = j, d = 240+j;
				front_arr = depth_arr[a:b, c:d]
				front_d = np.min(front_arr)
				f_count = (front_arr==front_d).sum()
				print "Again Searching...", front_d, f_count
				while front_d > 1.0:
					if f_count > 50:
						x_c = 120+j;
						x_0 = 320;
						y_c = 65 + i;
						y_0 = 200;
						yaw = arctan2((x_c-x_0),(y_C-y_0))
						print "Again Moving", front_d, f_count
						nav.velocity_set(0, 0.5*math.sin(yaw), -0.5*math.cos(yaw), 0.0, 0.5, False, False, True, True)
						return
					im_arr = focal*base/front_d
					front_d = focal*base/(im_arr-m)
					m = m+1;
					f_count = (front_arr==front_d).sum()



		print "Hold!"
		nav.position_hold()			

		# flag = False	


# Listener function to subscribe to ros topics
def listener():
	rospy.init_node('listener', anonymous=True)
	rospy.Subscriber("/flytsim/iris/camera_rgbd/camera_0/depth/image_raw", Image, callback)
	rospy.Subscriber("/stereo/left/image_rect_color", Image, left_callback)
	rospy.Subscriber("/flytsim/mavros/local_position/local", TwistStamped, pose)
	rospy.spin()

if __name__ == '__main__':
	listener()	
	
