# !/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError
count = 0;
pub = rospy.Publisher('chatter', Image, queue_size=10)

def callback(data):
	global count
	count = count + 1;
	#rospy.init_node('talker', anonymous=True)
	#rate = rospy.Rate(5) # hz
	#while not rospy.is_shutdown():
		#rospy.loginfo(data)
	if count == 3:
		pub.publish(data)
		count = 0;
	#rate.sleep()
	'''bridge = CvBridge()
	left_arr = bridge.imgmsg_to_cv2(image_a, desired_encoding="bgr8")	
	cv2.imshow("left_r", left_arr)
	if cv2.waitKey(1) and 0xFF == ord('q'):
		pass'''


# Listener function to subscribe to ros topics
def listener():
	rospy.init_node('listener', anonymous=True)
	rospy.Subscriber("/camera/image_raw", Image, callback)
	rospy.spin()


if __name__ == '__main__':
	listener()	
	
