# !/usr/bin/env python
import rospy
import numpy as np
from sensor_msgs.msg import Image
from stereo_msgs.msg import DisparityImage
import cv2
from cv_bridge import CvBridge, CvBridgeError


bridge = CvBridge()

def left_callback(left):
	global b, x1, y1, c
	left_arr = bridge.imgmsg_to_cv2(left, desired_encoding="bgr8")
	if len(b)> 100 and len(b)<10000:
		for i in range(0,len(b)-1):
			x1 = b[i-1,0]
			y1 = b[i-1,1]
			cv2.circle(left_arr, (y1, x1),15, (0, 255, 0), -1)
	if len(c)>10 and len(c)<10000:
		for i in range(0,len(c)-1):
			x2 = c[i-1,0]
			y2 = c[i-1,1]
		cv2.circle(left_arr, (y2, x2),15, (255, 0, 0), -1)
	cv2.putText(left_arr,"Min. Distance",(30,30), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,255,0),2)
	cv2.putText(left_arr,str(dist+0.01),(30,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,255,0),2)
	cv2.putText(left_arr,"Max. Distance",(400,30), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(255,0,0),2)
	cv2.putText(left_arr,str(dist1),(400,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(255,0,0),2)
	cv2.imshow("left1234", left_arr)
	if cv2.waitKey(1) and 0xFF == ord('q'):
		pass

def callback(data):
	global b,x1,y1, c, dist, dist1
	image_arr = bridge.imgmsg_to_cv2(data.image, desired_encoding="8UC1") # Converts the depth image into a 640 X 480 grid
	image_arr[image_arr==0] = 35;
	#image_arr[image_arr==1] = 35;
	#image_arr[image_arr>45] = 63;
	focal = data.f
	base = data.T
	depth_arr = focal*base/image_arr
	
	#depth_arr[depth_arr>33.0] = 4.555567;
	depth_arr[depth_arr>5.0] = 3.555567;
	#cv2.imshow("depth", image_arr)
	#cv2.moveWindow("depth",800,500)
	#if cv2.waitKey(1) and 0xFF == ord('q'):
	#	pass
	dist = np.min(depth_arr)
	a = zip(*np.where(depth_arr<(dist+0.01)))
	a = np.array(a)
	#np.delete(a, list(range(0, a.shape[1], 2)), axis=1)
	b = a[:,:-1]
	dist1 = np.max(depth_arr)
	c = zip(*np.where(depth_arr>(dist1-0.01)))
	c = np.array(c)
	print len(b), len(c)
	#print len(c)




def listener():
	rospy.init_node('listener', anonymous=True)
	rospy.Subscriber("/stereo/disparity", DisparityImage, callback)
	rospy.Subscriber("/stereo/left/image_rect_color", Image, left_callback)
	rospy.spin()

if __name__ == '__main__':
	listener()	
