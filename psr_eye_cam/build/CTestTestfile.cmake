# CMake generated Testfile for 
# Source directory: /home/flytpod/psr_eye_cam/src
# Build directory: /home/flytpod/psr_eye_cam/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(ps4eye)
SUBDIRS(image_temp)
SUBDIRS(gscam)
