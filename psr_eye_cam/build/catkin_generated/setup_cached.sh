#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export LD_LIBRARY_PATH="/home/flytpod/psr_eye_cam/devel/lib/arm-linux-gnueabihf:/flyt/flytos/flytcore/lib/arm-linux-gnueabihf:/opt/ros/indigo/lib/arm-linux-gnueabihf:/home/flytpod/psr_eye_cam/devel/lib:/flyt/flytos/flytcore/lib:/opt/ros/indigo/lib"
export PKG_CONFIG_PATH="/home/flytpod/psr_eye_cam/devel/lib/arm-linux-gnueabihf/pkgconfig:/flyt/flytos/flytcore/lib/arm-linux-gnueabihf/pkgconfig:/opt/ros/indigo/lib/arm-linux-gnueabihf/pkgconfig:/home/flytpod/psr_eye_cam/devel/lib/pkgconfig:/flyt/flytos/flytcore/lib/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PWD="/home/flytpod/psr_eye_cam/build"